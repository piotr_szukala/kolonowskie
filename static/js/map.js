// Określenie inicjacji mapy

// Dodanie window spowodowało, że zmienna map jest zmienną globalną i mogę się do jej elementóœ odnosić ze wszustkich plików
var map = window.L.map('map', {
  center: [50.6612,18.3687],
  zoom: 15,
  maxZoom: 18
});

////////////////////////////////////////////////////////////////////////////////////////////////////
// Deklarowanie warstw


// Dodanie warstw podkładu
osm = L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
}).addTo(map);

googleHybrid = L.tileLayer('http://{s}.google.com/vt/lyrs=s,h&x={x}&y={y}&z={z}',{
  maxZoom: 20,
  subdomains:['mt0','mt1','mt2','mt3']
});

googleTerrain = L.tileLayer('http://{s}.google.com/vt/lyrs=p&x={x}&y={y}&z={z}',{
  maxZoom: 20,
  subdomains:['mt0','mt1','mt2','mt3']
});



// Dodanie swoich WMS
var gmina = L.tileLayer.wms("http://gis.lucatel.pl:8080/geoserver/kolonowskie/gwc/service/wms?", {
  layers: 'gmina',
  format: 'image/png',
  transparent: true,
  tiled: true,
  attribution: "Lucatel Sp. z o.o."
}).addTo(map);



var slupy = L.tileLayer.wms("http://gis.lucatel.pl:8080/geoserver/kolonowskie/wms?", {
  layers: 'slupy',
  format: 'image/png',
  transparent: true,
  tiled: true,
  attribution: "Lucatel Sp. z o.o."
});


// Deklarowanie WMS z Geoportalu
var ortofotomapa = L.tileLayer.wms("http://mapy.geoportal.gov.pl/wss/service/img/guest/ORTO/MapServer/WMSServer", {
  layers: 'Raster',
  format: 'image/png'
});

var kataster = L.tileLayer.wms("http://mapy.geoportal.gov.pl/wss/service/pub/guest/G2_GO_WMS/MapServer/WMSServer", {
  layers: 'Dzialki,NumeryDzialek',
  format: 'image/png',
  transparent: true
});


// Definicja controlsów




////////////////////////////////////////////////////////////////////////////////////////////////////
 // Definicja legendy
var osm = {
  "Google Hybrid": googleHybrid,
  "Googlw Terrain": googleTerrain,
  "OpenStreetMap": osm

};
var infrastruktura = {
  "Geoportal": {
    'Ortofotomapa': ortofotomapa,
    'Kataster': kataster
  },
  "Sieć oświetleniowa": {
    '<img alt="legenda" src="http://gis.lucatel.pl:8080/geoserver/wms?REQUEST=GetLegendGraphic&VERSION=1.0.0&FORMAT=image/png&WIDTH=20&HEIGHT=20&LAYER=pawonkow:gmina&LEGEND_OPTIONS=forceLabels:on">': gmina,
    '<img alt="legenda" src="http://gis.lucatel.pl:8080/geoserver/wms?REQUEST=GetLegendGraphic&VERSION=1.0.0&FORMAT=image/png&WIDTH=20&HEIGHT=20&LAYER=pawonkow:slupy&LEGEND_OPTIONS=forceLabels:on">': slupy

  }
};

L.control.groupedLayers(osm, infrastruktura,{collapsed:false}).addTo(map);

