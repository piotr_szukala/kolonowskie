// Obsługa granic dla slupy o tym samym nr Stacji
////////////////////////////////////////////////////////////////////////////////////////////////////


// Zmenne
var boundsSlupy = null;
var geojsonSlupy = null; //Dodanie zmiennej na poziomie document umożliwiło odwołanie się do tej zmiennej w innym pliku
var geojsonStacje= null;
////////////////////////////////////////////////////////////////////////////////////////////////////



// Style obrysów słupów
var styleBoundsSlupy = {
    // radius: 8,
    // fillColor: "#1856C9",
    color: "#000",
    dashArray: '6',
    weight: 3,
    opacity: 0.5,
    fillOpacity: 0
};


// Funkcja zarządzająca geojsonem z obrysem. Na tym etapie jeszcze nie wczytuje do mapy!!
function handleJsonBounds(data) {
  //console.log(data);
  boundsSlupy = L.geoJson(data, {
    // Określenie stylów warstwy i ewenualnie opjcę onEachFeature, pointToLayer lub filter
    style: styleBoundsSlupy,
  })
  // boundsSlupy.addGeoJSON(data).addTo(map);
  // map.fitBounds(geojsonlayer.getBounds());
};

// Wysłanie zapytania do postgis w celu pozyskania obrysów i liczby słupów dla danej stacji
$.ajax({
  url: boundsSlupyUrl,
  type: 'GET',
  dataType: 'json',
  contentType: 'application/json',
  mimeType: 'application/json',
  async: true, //Spowodowało, że skrypt nie czeka aż pobrane zostaną dane z tego zapytania ajax.
  success: handleJsonBounds,
  error: function (data, status, er) {
    console.log(er);
  }
});


// Obsługa geojsona słupów
////////////////////////////////////////////////////////////////////////////////////////////////////
// Leci zapytanie do bazy danych po wszystki słupy ale warstwa nie wczytuje się do mapy. Dopiero kliknięcie na stację spowoduje pojawienie się tych słupów, które spełnią warunek


// Określenie stylu słupów
var myStyle = {
  radius: 5,
  fillColor: "#FFB200",
  color: "#000",
  // dashArray: '3',
  weight: 0.5,
  opacity: 1,
  fillOpacity: 0.8
};


// Akcja po najechaniu myszką na słup
function highlightFeature(e) {
  var layer = e.target; //określenie wskazanego elementu

  // var getFeature = e.target.feature.properties;

  // Można określić popup po najechaniu na obiekt
  // var popupContent = 'nr stacji: ' + getFeature.wyróżnik+'-'+getFeature.nr+'<br>'+'typ stacji: '+getFeature["typ stacji"]+'<br>'+getFeature["nazwa stac"]

  layer.setStyle({
    weight: 5,
    color: '#666',
    dashArray: '',
    fillOpacity: 0
  });

  // Pojawienie się popup
  // layer.bindPopup(popupContent, {offset: new L.Point(0, -5)}),
  // this.openPopup();
};


// Akcja po zjechaniu myszką z danego obiektu
function resetHighlight(e) {
  geojsonSlupy.resetStyle(e.target);

  // Zamknięcie popup
  // map.closePopup();
};


// Akcja po kliknięciu na obiekt
function mouseClick(e) {
  var layer = e.target; //definicja wskazanego obiektu
  var getFeature = e.target.feature.properties ; //definicja właściwości geojsona

  // Ustawienie treści popup
  var popupContent =
  'Historyczny numer słupa: <strong>' + getFeature.nr_slupa+'</strong><br>'+
  'Numer stacji: <strong>'+getFeature.id_stacji+'</strong><br>'+
  'Unikatowy numer słupa: <strong>' + getFeature.id_slupa + '</strong><br>'+
  '<a href="./foto/'+getFeature.foto1+'" data-lightbox="roadtrip"><img width="350" src="./foto/'+getFeature.foto1+'"/></a>'+
  '<a href="./foto/'+getFeature.foto2+'" data-lightbox="roadtrip">foto2</a>'+' / '+
  '<a href="./foto/'+getFeature.foto3+'" data-lightbox="roadtrip">foto3</a>'+' / '+
  '<a href="./foto/'+getFeature.foto4+'" data-lightbox="roadtrip">foto4</a>'+' / '+
  '<a href="./foto/'+getFeature.foto5+'" data-lightbox="roadtrip">foto5</a>'

  // Możliwość ustawienia stylu klikniętego obiektu
  // layer.setStyle({
  //   weight: 5,
  //   color: '#666',
  //   dashArray: '',
  //   fillOpacity: 0
  // }),

  // Wywołanie popup z offsetem, bo przysłaniał punkt
  layer.bindPopup(popupContent, {offset: new L.Point(0, -5)}),
  this.openPopup();
};


// Dodanie funkcjonalności dla każdego elementu
function onEachFeature(feature, layer) {

  // Dodanie etykiet (wymagana biblioteka LeafLet)
  // var label = new L.Label();
  // var labelContent = feature.properties.wyróżnik+'-'+feature.properties.nr
  // label.setContent(labelContent);
  // label.setLatLng(layer.getBounds().getCenter());
  // map.showLabel(label);

  layer.on({
    mouseover: highlightFeature,
    mouseout: resetHighlight,
    click: mouseClick
  });
};


// Obsługa geojsona
function handleJsonSlupy(data) {
  //console.log(data);

  geojsonSlupy = L.geoJson(data, {
    // Definicja warstwy punktowej jako punkt svg. Domyślnie jest marker
    pointToLayer: function (feature, latlng) {
      return L.circleMarker(latlng);
    },
    style: myStyle,
    onEachFeature: onEachFeature
  });
  map.fitBounds(geojsonSlupy.getBounds());
  // console.log(geojsonSlupy)
  // return geojsonSlupy
};


// Wysłanie zapytania do funkcji w app.py przez index.html
$.ajax({
  url: postGIS_slupyUrl,
  type: 'GET',
  dataType: 'json',
  contentType: 'application/json',
  mimeType: 'application/json',
  async: false, //Spowodowało, że skrypt czeka aż pobrane zostaną dane z tego zapytania ajax. Ważne jeśli chce się dalej wykorzystać zmienną globalną.
  success: handleJsonSlupy,
  error: function (data, status, er) {
    console.log(er);
  }
});

// console.log(geojsonSlupy) //Dzięki zastosowaniu async: false zmienna globalna się wypełni zapytaniem ajax
// Obsługa geojsona Stacje
////////////////////////////////////////////////////////////////////////////////////////////////////


var styleStacje = {
  radius: 8,
  fillColor: "#1856C9",
  color: "#000",
  // dashArray: '3',
  weight: 1.5,
  opacity: 0.8,
  fillOpacity: 0.6
}


// Funkcja po najechaniu myszką na obiekt
function boundDisplay(e) {
  var layer = e.target; //definiacja wskazanego obiektu
  var getNumber = layer.feature.properties.numer; //Atrybut klikniętego elementu

  // Lepiej ten fragment zmienić na filtr
  // Pojawienie się obrysów, które spełniają warunek. Można to chyba też zrobić metodą filter
  boundsSlupy.eachLayer( //eachLayer działa ale nie wiem czemu :P
    function(feature){
      map.removeLayer(feature) //Usuwa poprzednio wyświetlone elementy.
      if(feature.feature.properties.id_stacji==getNumber){ //Porównuje atrybuty klikniętego elementu z elementami z iteracji
        feature.addTo(map); //Jeśli warunek został spełniony to element zostaje dodany do mapy
        feature.bringToBack(); //Przerzucenie elementów na spód

        var iloscSlupow = feature.feature.properties.count; //Pobranie atrybutu
        var popupContent = "Ilość słupów: "+iloscSlupow; //popup
        feature.bindPopup(popupContent).openPopup();
        // map.fitBounds(feature.getBounds());
    }
  });


};


// Akcja po kliknięciu. Wyświetlenie innych elementów
function displaySlupy(e) {
  var numerStacji = e.target.feature.properties.numer

  geojsonSlupy.eachLayer(function(slupy){
    map.removeLayer(slupy);
    if (slupy.feature.properties.id_stacji == numerStacji) {
      slupy.addTo(map);
    };
  }),

  map.fitBounds(e.target.getBounds());
};


// Obsługa geojsona z app.py
function handleJsonStacje(data) {
  //console.log(data);
  // Zmiana punktów na svg zamiast domyślnych markerów
  geojsonStacje = L.geoJson(data, {
    pointToLayer: function (feature, latlng) {
      return L.circleMarker(latlng);
    },
    style: styleStacje,
    onEachFeature: function (feature, layer) {
      var label = new L.Label(); //Dodanie etykiet do elemetów geojsona
      var labelContent = feature.properties.numer
      label.setContent(labelContent);
      label.setLatLng(layer.getBounds().getCenter());
      map.showLabel(label);

      //Akcje na poszczególne interakcje z kursorem
      layer.on({
        mouseover: boundDisplay, //Wyświetlenie obrysów
        // mouseout: resetBounds,
        click: displaySlupy //Wyświetlenie słupów
      });
    }
  }).addTo(map);

};


// Zapytanie SQL do pythona
$.ajax({
  url: postGIS_stacjeUrl,
  type: 'GET',
  dataType: 'json',
  contentType: 'application/json',
  mimeType: 'application/json',
  success: handleJsonStacje,
  error: function (data, status, er) {
    console.log(er);
  }
});

