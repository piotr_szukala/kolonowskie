Aplikacja webowa do zarządzania słupami z oświetleniem w gminie Kolonowskie.

Funkcjonalność:
1. Wczytywanie danych bezpośrednio z postGIS
2. Wczytywanie z postGIS ConvexHull, czyli obwiedni dookoła słupów o tym samym atrybucie stacji.
3. Zliczenie słupów, które należą do danej stacji i są własnością gminy.
4. Po kliknięciu na stację wyświetlają się  dopiero słupy co pozwala ograniczyć ilkość wyświetlanych danych wektorowych i przyspiesza pracę aplikacji.
5. Po kliknięciu na słup wyświetla się popup ze zdjęciem a po kliknięciu na zdjęcie pojawia się lightbox

TODO:
1. Logownie użytkoników
2. Możliwość edycji danych porzez przeglądarkę dla zalogowanych użytkowników
3. Stworzenie uniwersalnej aplikacji, która będzie listowała gminy dla każdego użytkownika z możliwością edycji danych. (nie wiem czy to jest dobry pomysł bo struktura danych jest różna i odnosi się do różnych tabel)
