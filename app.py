#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-

import sys
reload(sys)
sys.setdefaultencoding("utf-8")
from flask import Flask, render_template, request, json, redirect, session, url_for, Response, jsonify
import psycopg2


app = Flask(__name__)

app.debug = True

app.config['SECRET_KEY'] = 'secretOfTheRunes'
# app.secret_key = 'why would I tell you my secret key?'

app.config['DATABASE_NAME'] = 'kolonowskie' #nazwa bazy danych
app.config['DATABASE_HOST'] = '178.216.200.164' #host bazy danych
app.config['DATABASE_USER'] = 'postgres'
app.config['DATABASE_PASS'] = 'lucatel6/7A'




@app.route("/")
def main():
    return render_template('login_form.html')


@app.route("/validateLogin", methods=['POST'])
def validateLogin():

    try:
        _name = request.form['inputName']
        _password = request.form['inputPassword']

        # Polaczenie z baza danych mysql
        conn = psycopg2.connect(database='global',user=app.config['DATABASE_USER'],host=app.config['DATABASE_HOST'],password=app.config['DATABASE_PASS'],port=5432)
        # conn = mysql.connect()
        cursor = conn.cursor()
        query = "SELECT * from users where email = '%s' and password = '%s'" % (_name, _password)
        cursor.execute(query)
        data = cursor.fetchall()


        if str(data[0][4]) == _password and str(data[0][3]) == _name:
            # imie_nazwisko = data[0][2]
            user_name = data[0][1]
            session['user'] = data[0][0]
            if data[0][5] == 1:
                return redirect(url_for('userHome',
                    user_name = user_name))
                # return redirect(url_for('adminHome',
                # user_name = user_name))
            else:
                return 'OK2'
                # return redirect(url_for('userHome',
                # user_name = user_name))
        else:
            error = u'Nieprawidłowy email lub hasło'
            return error
    except:
        error = u'Nieprawidłowy email lub hasło'
        return error


    finally:
        cursor.close()
        conn.close()


@app.route("/<user_name>")
def userHome(user_name):
    if session.get('user'):
        return render_template('user.html')
    #     try:
    #         # Polaczenie z baza danych mysql
    #         conn = psycopg2.connect(database=app.config['DATABASE_NAME'],user=app.config['DATABASE_USER'],host=app.config['DATABASE_HOST'],password=app.config['DATABASE_PASS'],port=5432)

    #         cursor = conn.cursor()
    #         query = "SELECT kob.gm_nazwa, kob.kob, users.imie_nazwisko from users join users_kob on (users_kob.users_id = users.id) join kob on (users_kob.kob_id = kob.id) where user_name = '%s';" % user_name
    #         cursor.execute(query)
    #         data = cursor.fetchall()

    #         user_name = data[0][2]
    #         # Wydobycie nazw z odpowiedzi sql. Surowa odpowiedz to tuple tupli

    #         kob_dict = dict() #Aby to zadzialalo musialem dodac () po dict. Samo dict () dzialalo tylko z kob_dict = {k:v}
    #         for name in data:
    #             k = name[0]
    #             v = name[1]
    #             kob_dict[k] = v


    #         # layer_wms = str('linie_jdf')


    #         return render_template('userHome.html',
    #             user_name=user_name,
    #             # layer_wms=layer_wms,
    #             kob_dict=kob_dict
    #             )
    #     except:
    #         error = u'Nieprawidłowy email lub hasło'
    #         return render_template('signin.html',error = error)
    #     finally:
    #         cursor.close()
    #         conn.close()

    # else:




@app.route("/postGIS_slupy", methods=['GET'])
def postGIS_slupy():
    try:

        conn = psycopg2.connect(database=app.config['DATABASE_NAME'],user=app.config['DATABASE_USER'],host=app.config['DATABASE_HOST'],password=app.config['DATABASE_PASS'],port=5432)

        cursor = conn.cursor()
        cursor.execute("""SELECT row_to_json(fc) FROM ( SELECT 'FeatureCollection' As type, array_to_json(array_agg(f)) As features FROM (SELECT 'Feature' As type, ST_AsGeoJSON(lg.geom)::json As geometry, row_to_json((SELECT l FROM (SELECT nr_slupa, id_stacji, id_slupa, foto1, foto2, foto3, foto4, foto5) As l)) As properties  FROM "slupy" As lg WHERE wlasnosc = 'gmina') As f)  As fc;""")

        conn.commit()
        data = cursor.fetchall()[0][0]
        # data = str(data)
        cursor.close()
        conn.close()
        # jsonify(result=data[0][0])
        # json.dumps(data) # Działa na serwerze!!!!
        # data # Działa lokalnie
        # Response(response=data, status=200, mimetype="application/json")
        return data
    except:
        return 'Cos poszlo nie tak'


@app.route("/postGIS_stacje", methods=['GET'])
def postGIS_stacje():
    try:

        conn = psycopg2.connect(database=app.config['DATABASE_NAME'],user=app.config['DATABASE_USER'],host=app.config['DATABASE_HOST'],password=app.config['DATABASE_PASS'],port=5432)

        cursor = conn.cursor()
        cursor.execute("""SELECT row_to_json(fc) FROM ( SELECT 'FeatureCollection' As type, array_to_json(array_agg(f)) As features FROM (SELECT 'Feature' As type, ST_AsGeoJSON(lg.geom)::json As geometry, row_to_json((SELECT l FROM (SELECT numer) As l )) As properties  FROM "stacje" As lg   ) As f )  As fc;""")

        conn.commit()
        data = cursor.fetchall()[0][0]
        cursor.close()
        conn.close()
        # jsonify(result=data[0][0])
        # json.dumps(data) # Działa na serwerze!!!!
        # data # Działa lokalnie
        # Response(response=data, status=200, mimetype="application/json")
        return data
    except:
        return 'Cos poszlo nie tak'

@app.route("/boundsSlupy", methods=['GET'])
def boundsSlupy():
    try:

        conn = psycopg2.connect(database=app.config['DATABASE_NAME'],user=app.config['DATABASE_USER'],host=app.config['DATABASE_HOST'],password=app.config['DATABASE_PASS'],port=5432)

        cursor = conn.cursor()
        cursor.execute("""SELECT row_to_json(fc) FROM ( SELECT 'FeatureCollection' As type, array_to_json(array_agg(f)) As features FROM (SELECT 'Feature' As type, ST_AsGeoJSON(ST_ConvexHull(ST_Collect(lg.geom)))::json As geometry, row_to_json((SELECT l FROM (SELECT lg.id_stacji, COUNT(lg.id_stacji)) As l )) As properties  FROM "slupy" As lg WHERE wlasnosc = 'gmina' GROUP BY id_stacji)  As f)  As fc;""")


        conn.commit()
        data = cursor.fetchall()[0][0]
        cursor.close()
        conn.close()
        # jsonify(result=data[0][0])
        # json.dumps(data) # Działa na serwerze!!!!
        # data # Działa lokalnie
        # Response(response=data, status=200, mimetype="application/json")
        return data
    except:
        return 'Cos poszlo nie tak'

@app.route('/logout')
def logout():
    session.pop('user',None)
    return redirect('/')

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5001)
